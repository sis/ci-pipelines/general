# General

This pipeline checks:
 - No passwords stored in the git repository


## How to find the password

Unfortunately, gitlab keeps the easy way to find the issue for Ultimate customers.
To download the report, you can either download the report from a merge request or go to `pipeline` and download the report from the button on the right of the pipeline.
